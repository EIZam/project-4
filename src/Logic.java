public class Logic {
    public static void main(String[] args) {
        boolean value1 = true, value2 = false, result;
        System.out.println(value1);
        System.out.println(!value1);
        System.out.println(value1 && value2);
        result = 5 > 6;
        System.out.println(result);
        int number;
        number = result ? 1 : 2;
        System.out.println(number);
    }
}
